package com.urovo.i9000s.api.jni;

import android.R.integer;

/**
 * Created by lenovo on 2017/11/13.
 */

public class DukptJni {

	//三套密钥管理
	public interface mKeyType {
	    int MSR_KEY     = 1;
	    int EMV_KEY     = 2;
	    int PIN_KEY     = 3;
	}

	//mode1:pin;  mode2:mac;  mode3:data
	public interface mMode {
	    int mode1    = 1;
	    int mode2    = 2;
	    int mode3    = 3;
	}


	/*
	*
	*  Below is an example of KSN, BDK, generated IPEK
	    KSN:  11111746011BED
	    BDK:  17F3EE7E 194E0672 0391F95D 266A73F2
	    IPEK: F706A15B E612755A F6556CAC 41258088
	    Also test data:

	    KSN: 11111746011BEDE00001
	    Encrypted data: 698E492C 5B280508 9E52F371 276CA1CD
	    Decrypted data: 0123456789ABCDEFFEDCBA9876543210
	*
	*
	*
	* */


	static {
		System.loadLibrary("DukptJni");
	}

	public native static int DukptInitial(int iKeyType , byte[] bsKsn , int iKsnLen , byte[] bsIpek , int iIpekLen);

	public native static int DukptInitialEx(byte[] bsBdk ,int iBdkLen ,int iKeyType ,byte[] bsKsn ,int iKsnLen ,byte[] bsIpek ,int iIpekLen);

}
