//package com.urovo.myapplication;//
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by Fernflower decompiler)
////
//
//
//import android.device.MagManager;
//import android.device.SEManager;
//import android.device.SEManager.PedInputListener;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.jniexport.UROPElibJni;
//import com.urovo.i9000s.api.emv.ATR;
//import com.urovo.i9000s.api.emv.CAPK;
//import com.urovo.i9000s.api.emv.ContantPara.AccountSelectionResult;
//import com.urovo.i9000s.api.emv.ContantPara.CheckCardMode;
//import com.urovo.i9000s.api.emv.ContantPara.CheckCardResult;
//import com.urovo.i9000s.api.emv.ContantPara.CurrencyCharacter;
//import com.urovo.i9000s.api.emv.ContantPara.DisplayText;
//import com.urovo.i9000s.api.emv.ContantPara.EmvOption;
//import com.urovo.i9000s.api.emv.ContantPara.Error;
//import com.urovo.i9000s.api.emv.ContantPara.PinEntryResult;
//import com.urovo.i9000s.api.emv.ContantPara.PinEntrySource;
//import com.urovo.i9000s.api.emv.ContantPara.TerminalSettingStatus;
//import com.urovo.i9000s.api.emv.ContantPara.TransactionResult;
//import com.urovo.i9000s.api.emv.ContantPara.TransactionType;
//
//import com.urovo.i9000s.api.emv.EmvJni.JniEmvListener;
//import com.urovo.i9000s.api.emv.EmvListener;
//import com.urovo.i9000s.api.emv.Funs;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Hashtable;
//import java.util.List;
//
//public class MyEmvApi implements JniEmvListener {
//    private static final String ACTIVITY_TAG = "emvlog";
//    public byte T9C = 0;
//    public long TransAmount = 0L;
//    public long backamt;
//    public byte[] TransCurrCode = new byte[6];
//    public int g_iret = 0;
//    public int g_CVMTYPE = 0;
//    public PinEntryResult g_PinEntryPro;
//    public static int g_checkCardRun = 0;
//    public static final int BUILD_INDIA = 0;
//    public static final int BUILD_NORMAL = 1;
//    public static final int BUILD_TYPE = 1;
//    public byte _NEED_SIGNED = 48;
//    public byte _NEED_SHOW_CERT = 49;
//    public byte _NEED_INPUT_ONLINE_PIN = 50;
//    public byte _NEED_INPUT_OFFPLAIN_PIN = 51;
//    public byte _NEED_INPUT_OFFENCRY_PIN = 52;
//    private EmvListener mEmvListener;
//    SEManager mSEManager;
//    Bundle param;
//    int pinType = 0;
//    private PedInputListener mPedInputListenerOFP = new PedInputListener() {
//        public void onChanged(int i, int i1, byte[] bytes) {
//            if (i == 0) {
//                if (MyEmvApi.this.pinType == 1) {
//                    UROPElibJni.ProcOfflinePlantPin(bytes, i1);
//                    MyEmvApi.this.procCVMcontinue();
//                } else if (MyEmvApi.this.pinType == 2) {
//                    UROPElibJni.ProcEncryptPin(bytes, i1);
//                    MyEmvApi.this.procCVMcontinue();
//                }
//            } else if (i == 1) {
//                MyEmvApi.this.onReturnTransactionResult(TransactionResult.CANCELED_OR_TIMEOUT);
//            }
//
//        }
//    };
//    static int icoutMG = 0;
//
//    public MyEmvApi(EmvListener mEmvListener) {
//        this.mEmvListener = mEmvListener;
//        this.initEmv("/sdcard/UROPE/");
//    }
//
//    private void checkMaginEMVfallBack() {
//        MagManager mMagManager = new MagManager();
//        Hashtable<String, String> dataout = new Hashtable();
//        icoutMG = 0;
//        this.g_iret = 0;
//        g_checkCardRun = 0;
//        byte[] buf = new byte[100];
//        mMagManager.open();
//        long lcheckCardTimeout = 30000L;
//        Funs.startTimer(lcheckCardTimeout);
//
//        do {
//            byte[] StripInfo = new byte[1024];
//            byte[] CardNo = new byte[20];
//            byte[] KSN = new byte[11];
//            byte[] chipTag = new byte[2];
//            if (this.g_iret == 1) {
//                this.onError(Error.DEVICE_BUSY, "cancel");
//                return;
//            }
//
//            int iret = mMagManager.checkCard();
//            if (iret == 0) {
//                int allLen = mMagManager.getAllStripInfo(StripInfo);
//                if (allLen > 8) {
//                    int len = StripInfo[1];
//
//                    int i;
//                    for(i = 1; i < 24 && StripInfo[4 + len + i] != 61; ++i) {
//                        ;
//                    }
//
//                    chipTag[0] = StripInfo[4 + len + i + 5];
//                    System.arraycopy(StripInfo, 4 + len, CardNo, 0, i);
//                }
//
//                if (allLen >= 16) {
//                    dataout = new Hashtable();
//                    dataout.put("StripInfo", Funs.bytes2HexString(StripInfo, allLen));
//                    dataout.put("CardNo", (new String(CardNo)).trim());
//                    this.onReturnCheckCardResult(CheckCardResult.MSR, dataout);
//                    return;
//                }
//
//                dataout = new Hashtable();
//                this.onReturnCheckCardResult(CheckCardResult.BAD_SWIPE, dataout);
//            }
//        } while(0 != Funs.checkTimer());
//
//        Log.i("maxlog", "time out");
//        this.onReturnCheckCardResult(CheckCardResult.TIMEOUT, dataout);
//    }
//
//    private void process_selectAppRet(int iret) {
//        switch(iret) {
//            case -230:
//                this.onReturnTransactionResult(TransactionResult.SELECT_APP_FAIL);
//                break;
//            case -217:
//                this.onReturnTransactionResult(TransactionResult.APPLICATION_BLOCKED_APP_FAIL);
//                break;
//            case -216:
//                this.onReturnTransactionResult(TransactionResult.NO_EMV_APPS);
//                this.onCheckCardisOut();
//                this.checkMaginEMVfallBack();
//                break;
//            case -214:
//                this.onReturnTransactionResult(TransactionResult.CARD_BLOCKED_APP_FAIL);
//                break;
//            default:
//                this.onReturnTransactionResult(TransactionResult.SELECT_APP_FAIL);
//        }
//
//    }
//
//    private int initEmv(String FILEPATH) {
//        int ret = 0;
//        int iflag = 0;
//        String[] CapkList = new String[]{"9F0605A0000003339F220103DF050420241231DF0281B0B0627DEE87864F9C18C13B9A1F025448BF13C58380C91F4CEBA9F9BCB214FF8414E9B59D6ABA10F941C7331768F47B2127907D857FA39AAF8CE02045DD01619D689EE731C551159BE7EB2D51A372FF56B556E5CB2FDE36E23073A44CA215D6C26CA68847B388E39520E0026E62294B557D6470440CA0AEFC9438C923AEC9B2098D6D3A1AF5E8B1DE36F4B53040109D89B77CAFAF70C26C601ABDF59EEC0FDC8A99089140CD2E817E335175B03B7AA33DDF040103DF031487F0CD7C0E86F38F89A66F8C47071A8B88586F26BF010131DF070101"};
//        String[] AIDList = new String[]{"9F0608A000000333010101DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000000DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000001000000DF1906000001000000DF2006000001000000DF2106000001000000", "9F0608A000000333010102DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000000DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000001000000DF1906000001000000DF2006000001000000DF2106000001000000", "9F0608A000000333010103DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000000DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000001000000DF1906000001000000DF2006000001000000DF2106000001000000"};
//        String dirPath = "/sdcard/UROPE/";
//        File dir = new File(dirPath);
//        if (!dir.exists()) {
//            dir.mkdirs();
//        } else {
//            iflag = 1;
//        }
//
//        UROPElibJni.SETPath(dirPath.getBytes(), dirPath.getBytes().length);
//        byte[] bvers = new byte[100];
//        UROPElibJni.GetVers(bvers);
//        UROPElibJni.GMax3250Open();
//        UROPElibJni.LogOutEnable(1);
//        UROPElibJni.ICCOpen();
//        UROPElibJni.MagCardOpen();
//        UROPElibJni.PEDatasInit();
//        if (iflag) {
//            return 0;
//        } else {
//            byte[] ASC = null;
//            byte[] BCD = null;
//            int ret = UROPElibJni.ClearAID();
//            ret = UROPElibJni.ClearCAPK();
//            int len = CapkList.length;
//
//            int i;
//            byte[] ASC;
//            byte[] BCD;
//            for(i = 0; i < len; ++i) {
//                ASC = CapkList[i].getBytes();
//                BCD = new byte[ASC.length / 2];
//                Funs.AscToBcd(BCD, ASC, ASC.length);
//                ret = UROPElibJni.updateCAPK(BCD, BCD.length);
//                Log.i("emvlog", "updateCAPK ret:" + ret);
//            }
//
//            len = AIDList.length;
//
//            for(i = 0; i < len; ++i) {
//                ASC = AIDList[i].getBytes();
//                BCD = new byte[ASC.length / 2];
//                Funs.AscToBcd(BCD, ASC, ASC.length);
//                ret = UROPElibJni.updateAID(BCD, BCD.length);
//            }
//
//            UROPElibJni.initTermConfig(new byte[]{-32, -31, -56}, new byte[]{96, 0, -16, -96, 1}, "12345678".getBytes(), "12345678".length(), new byte[]{3, 86}, "88888888".getBytes(), (byte)34, new byte[]{3, 86}, (byte)2, (byte)1, (byte)1, (byte)1, (byte)1, (byte)0);
//
//            try {
//                UROPElibJni.doit();
//            } catch (Exception var13) {
//                System.out.println("In Java:\n\t" + var13);
//            }
//
//            return ret;
//        }
//    }
//
//    public void startEmv(Hashtable<String, Object> data) {
//        this.g_iret = 0;
//        Object localObject1 = (EmvOption)data.get("emvOption");
//        byte isForceOnline = 0;
//        if (localObject1 == EmvOption.START) {
//            isForceOnline = 0;
//        } else if (localObject1 == EmvOption.START_WITH_FORCE_ONLINE) {
//            isForceOnline = 1;
//        }
//
//        String string1;
//        Object object;
//        if (data.containsKey("amount")) {
//            object = data.get("amount");
//            string1 = object.toString();
//            Double x = Double.parseDouble(string1);
//            this.TransAmount = (long)(x * 100.0D);
//        } else {
//            this.onRequestSetAmount();
//            if (this.g_iret == 1) {
//                return;
//            }
//        }
//
//        if (data.containsKey("cashbackAmount")) {
//            object = data.get("cashbackAmount");
//            string1 = object.toString();
//            this.backamt = Long.valueOf(string1);
//        } else {
//            this.backamt = 0L;
//        }
//
//        long lcheckCardTimeout = 30L;
//        if (data.containsKey("checkCardTimeout")) {
//            object = data.get("checkCardTimeout");
//            string1 = object.toString();
//            Double x = Double.parseDouble(string1);
//            lcheckCardTimeout = (long)(x * 100.0D);
//        }
//
//        byte[] subByte;
//        byte[] retByte;
//        if (data.containsKey("currencyCode")) {
//            object = data.get("currencyCode");
//            string1 = (String)object;
//            retByte = string1.getBytes();
//            subByte = new byte[6];
//            subByte[0] = 48;
//            int i = false;
//
//            for(int i = 0; i < 3; ++i) {
//                subByte[i + 1] = retByte[i];
//            }
//
//            Funs.AscToBcd(this.TransCurrCode, subByte, 4);
//            UROPElibJni.setCurrencyCode(this.TransCurrCode);
//        }
//
//        byte[] argicc;
//        if (g_checkCardRun == 0) {
//            CheckCardMode mCheckCardMode = CheckCardMode.SWIPE_OR_INSERT;
//            CheckCardResult mCheckCardResult = CheckCardResult.NO_CARD;
//            if (data.containsKey("checkCardMode")) {
//                object = data.get("checkCardMode");
//                mCheckCardMode = (CheckCardMode)object;
//                if (mCheckCardMode == CheckCardMode.SWIPE_OR_INSERT) {
//                    ;
//                }
//            }
//
//            argicc = new byte[4];
//            if (lcheckCardTimeout > 0L && lcheckCardTimeout < 100L) {
//                lcheckCardTimeout *= 1000L;
//            } else {
//                lcheckCardTimeout = 30000L;
//            }
//
//            Funs.startTimer(lcheckCardTimeout);
//
//            while(true) {
//                try {
//                    Thread.sleep(100L);
//                } catch (InterruptedException var19) {
//                    var19.printStackTrace();
//                }
//
//                if ((mCheckCardMode == CheckCardMode.SWIPE_OR_INSERT || mCheckCardMode == CheckCardMode.INSERT || mCheckCardMode == CheckCardMode.INSERT_OR_TAP || mCheckCardMode == CheckCardMode.SWIPE_OR_INSERT_OR_TAP) && UROPElibJni.ICCCheck(argicc) == 0) {
//                    mCheckCardResult = CheckCardResult.INSERTED_CARD;
//                    break;
//                }
//
//                if (0 == Funs.checkTimer()) {
//                    this.onError(Error.DEVICE_BUSY, "time out");
//                    return;
//                }
//
//                if (this.g_iret == 1) {
//                    this.onError(Error.DEVICE_BUSY, "cancel");
//                    return;
//                }
//            }
//        }
//
//        UROPElibJni.initTrans((byte)0, (byte)1, (byte)0, (byte)0, this.T9C, isForceOnline, this.TransAmount, this.backamt);
//        int ret = UROPElibJni.CreateAppLists();
//        if (ret != 0) {
//            if (ret != -201 && ret != -202) {
//                this.process_selectAppRet(ret);
//            } else {
//                this.onRequestDisplayText(DisplayText.USE_MAG_STRIPE);
//            }
//
//        } else {
//            retByte = new byte[256];
//            subByte = new byte[5];
//            argicc = new byte[50];
//            int index_start = 0;
//            int index_end = false;
//            int appnamelen = false;
//            ArrayList list = new ArrayList();
//            subByte[0] = 13;
//            subByte[1] = 10;
//            ret = UROPElibJni.GetManyApps(retByte);
//            if (ret == 0) {
//                this.onReturnTransactionResult(TransactionResult.NO_EMV_APPS);
//            } else {
//                int i;
//                for(i = 0; i < 7; ++i) {
//                    int index_end = Funs.findSub(retByte, index_start, retByte.length, subByte, 2);
//                    if (index_end == -1) {
//                        break;
//                    }
//
//                    int appnamelen = index_end - index_start;
//                    Arrays.fill(argicc, (byte)0);
//                    System.arraycopy(retByte, index_start, argicc, 0, appnamelen);
//                    byte[] appByte = new byte[appnamelen];
//                    System.arraycopy(argicc, 0, appByte, 0, appnamelen);
//                    String app1 = new String(appByte);
//                    index_start += appnamelen + 2;
//                    index_end += 2;
//                    list.add(app1);
//                }
//
//                if (i == 0) {
//                    this.onReturnTransactionResult(TransactionResult.NO_EMV_APPS);
//                } else {
//                    this.g_iret = 1;
//                    this.onRequestSelectApplication(list);
//                }
//            }
//        }
//    }
//
//    public int procCVMcontinue() {
//        int ret = UROPElibJni.CVMContinue();
//        if (ret != 0) {
//            this.g_CVMTYPE = ret;
//            if (ret == 50) {
//                this.onRequestPinEntry(PinEntrySource.KEYPAD);
//                return 0;
//            }
//
//            if (ret == 51) {
//                this.emv_proc_offplain();
//                return 0;
//            }
//
//            if (ret == 52) {
//                this.emv_proc_offencryp();
//                return 0;
//            }
//        }
//
//        this.emv_proc_afterEnterPin();
//        return 0;
//    }
//
//    public void emv_proc_offplain() {
//        this.mSEManager = new SEManager();
//        this.param = new Bundle();
//        this.param.putInt("KeyUsage", 3);
//        this.param.putInt("PINKeyNo", 1);
//        this.param.putInt("pinAlgMode", 1);
//        this.param.putString("cardNo", "6210817200002086930");
//        this.param.putBoolean("sound", true);
//        this.param.putBoolean("onlinePin", false);
//        this.param.putBoolean("FullScreen", true);
//        this.param.putLong("timeOutMS", 60000L);
//        this.param.putString("supportPinLen", "0,4,6,8,10,12");
//        this.param.putString("title", "Security Keyboard");
//        this.param.putString("message", "please input password ");
//        this.pinType = 1;
//        this.mSEManager.getPinBlockEx(this.param, this.mPedInputListenerOFP);
//    }
//
//    public void emv_proc_offencryp() {
//        SEManager mSEManager = new SEManager();
//        Bundle param = new Bundle();
//        param.putInt("KeyUsage", 3);
//        param.putInt("PINKeyNo", 1);
//        param.putInt("pinAlgMode", 1);
//        param.putString("cardNo", "6210817200002086930");
//        param.putBoolean("sound", true);
//        param.putBoolean("onlinePin", false);
//        param.putBoolean("FullScreen", true);
//        param.putLong("timeOutMS", 60000L);
//        param.putString("supportPinLen", "0,4,6,8,10,12");
//        param.putString("title", "Security Keyboard");
//        param.putString("message", "please input password ");
//        this.pinType = 2;
//        mSEManager.getPinBlockEx(param, this.mPedInputListenerOFP);
//    }
//
//    public void emv_proc_afterEnterPin() {
//        int ret = UROPElibJni.ProcRiskActAnalyse();
//        byte[] ICdatas;
//        if (ret == 204) {
//            ICdatas = new byte[12];
//            ret = UROPElibJni.GetTlv(40825, ICdatas);
//            if (ret > 0) {
//                Log.i("GetF55ForOnlineTx:", "UROPElibJni.GetTlv(0x9f79)");
//            } else {
//                if (this.g_CVMTYPE == this._NEED_SIGNED) {
//                    this.onRequestDisplayText(DisplayText.APPROVED_PLEASE_SIGN);
//                }
//
//            }
//        } else if (ret == 223) {
//            ICdatas = new byte[520];
//            byte[] track = new byte[200];
//            int[] tracklen = new int[2];
//            byte[] ksn = new byte[20];
//            int[] IClen = new int[2];
//            ret = UROPElibJni.GetF55ForOnlineTx(ICdatas, IClen);
//            if (ret == 0) {
//                this.onRequestOnlineProcess(Funs.bytes2HexString(ICdatas, IClen[0]), (String)null);
//            } else {
//                this.onReturnTransactionResult(TransactionResult.DECLINED);
//            }
//        } else if (ret == -229) {
//            this.onReturnTransactionResult(TransactionResult.DECLINED);
//        } else if (ret == -225) {
//            this.onReturnTransactionResult(TransactionResult.DECLINED);
//        } else {
//            this.onReturnTransactionResult(TransactionResult.TERMINATED);
//        }
//    }
//
//    public String getValByTag(int tag) {
//        String strVal = "";
//        byte[] buff = new byte[256];
//        int retlen = UROPElibJni.GetTlv(tag, buff);
//        if (retlen == -1) {
//            return strVal;
//        } else {
//            strVal = Funs.bytes2HexString(buff, retlen);
//            return strVal;
//        }
//    }
//
//    public String getTlvByTagLists(List<String> TagList) {
//        String strTlv = "";
//        String lenStr = "";
//
//        for(int i = 0; i < TagList.size(); ++i) {
//            String strTag = (String)TagList.get(i);
//            int iTag = Integer.parseInt(strTag, 16);
//            String strVal = this.getValByTag(iTag);
//            if (!strVal.isEmpty()) {
//                strTlv = strTlv + strTag;
//                lenStr = Integer.toHexString(strVal.length() / 2);
//                if (lenStr.length() < 2) {
//                    strTlv = strTlv + "0";
//                    strTlv = strTlv + lenStr;
//                } else {
//                    strTlv = strTlv + lenStr;
//                }
//
//                strTlv = strTlv + strVal;
//            }
//        }
//
//        return strTlv;
//    }
//
//    public void sendTerminalTime(String terminalTime) {
//    }
//
//    public boolean setAmount(String amount, String cashbackAmount, String currencyCode, TransactionType transactionType, CurrencyCharacter[] currencyCharacters) {
//        this.TransAmount = Long.valueOf(amount);
//        this.backamt = Long.valueOf(cashbackAmount);
//        byte[] srtbyte = currencyCode.getBytes();
//        byte[] tmpCode = new byte[6];
//        tmpCode[0] = 48;
//        int i = false;
//
//        for(int i = 0; i < 3; ++i) {
//            tmpCode[i + 1] = srtbyte[i];
//        }
//
//        Funs.AscToBcd(this.TransCurrCode, tmpCode, 4);
//        UROPElibJni.setCurrencyCode(this.TransCurrCode);
//        return true;
//    }
//
//    public void cancelSetAmount() {
//        this.g_iret = 1;
//    }
//
//    public void cancelCheckCard() {
//        this.g_iret = 1;
//    }
//
//    public void selectApplication(int index) {
//        int ret = false;
//        int ret = UROPElibJni.AppSel(index);
//        this.g_iret = ret;
//        if (this.g_iret != 0) {
//            this.process_selectAppRet(this.g_iret);
//        } else {
//            ret = UROPElibJni.ReadApp();
//            if (ret != 0) {
//                this.onReturnTransactionResult(TransactionResult.INVALID_ICC_DATA);
//            } else {
//                this.g_iret = 1;
//                this.onRequestConfirmCardno();
//                if (this.g_iret == 1) {
//                    this.onReturnTransactionResult(TransactionResult.CANCELED);
//                } else {
//                    ret = UROPElibJni.CardAuth();
//                    if (ret != 0) {
//                        this.onReturnTransactionResult(TransactionResult.TERMINATED);
//                    } else {
//                        UROPElibJni.ProcRestric();
//                        this.g_iret = 1;
//                        this.onRequestFinalConfirm();
//                        if (this.g_iret == 1) {
//                            this.onReturnTransactionResult(TransactionResult.CANCELED);
//                        } else {
//                            this.g_CVMTYPE = 0;
//                            ret = UROPElibJni.CVMStart();
//                            if (ret != 0) {
//                                this.g_CVMTYPE = ret;
//                                if (ret == 50) {
//                                    this.onRequestPinEntry(PinEntrySource.KEYPAD);
//                                    return;
//                                }
//
//                                if (ret == 51) {
//                                    this.emv_proc_offplain();
//                                    return;
//                                }
//
//                                if (ret == 52) {
//                                    this.emv_proc_offencryp();
//                                    return;
//                                }
//                            }
//
//                            this.emv_proc_afterEnterPin();
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public void cancelSelectApplication() {
//        this.g_iret = 1;
//    }
//
//    public void sendConfirmCardnoResult(boolean isConfirmed) {
//        if (isConfirmed) {
//            this.g_iret = 0;
//        } else if (!isConfirmed) {
//            this.g_iret = 1;
//        }
//
//    }
//
//    public void sendFinalConfirmResult(boolean isConfirmed) {
//        if (isConfirmed) {
//            this.g_iret = 0;
//        } else if (!isConfirmed) {
//            this.g_iret = 1;
//        }
//
//    }
//
//    public void sendFinalConfirmResult(boolean isConfirmed, String tlv) {
//        if (isConfirmed) {
//            this.g_iret = 0;
//        } else if (!isConfirmed) {
//            this.g_iret = 1;
//        }
//
//    }
//
//    public int sendOnlineProcessResult(String tlv) {
//        byte[] buff = Funs.StrToHexByte(tlv);
//        int iret = false;
//        int iret = UROPElibJni.SetProcF55(buff, tlv.length() / 2);
//        if (iret != 0) {
//            this.onReturnTransactionResult(TransactionResult.DECLINED);
//        } else {
//            if (this.g_CVMTYPE == this._NEED_SIGNED) {
//                this.onRequestDisplayText(DisplayText.APPROVED_PLEASE_SIGN);
//            }
//
//            this.onReturnTransactionResult(TransactionResult.APPROVED);
//        }
//
//        return iret;
//    }
//
//    public void enableInputAmount(Hashtable<String, Object> data) {
//    }
//
//    public void disableInputAmount() {
//    }
//
//    public void cancelPinEntry() {
//        this.g_PinEntryPro = PinEntryResult.CANCEL;
//        Log.i("maxlog", "PinEntryResult.CANCEL");
//        this.g_iret = 1;
//    }
//
//    public void sendPinEntryResult(String pin) {
//        this.g_PinEntryPro = PinEntryResult.ENTERED;
//        UROPElibJni.SetPinCVR(1);
//        this.procCVMcontinue();
//    }
//
//    public void bypassPinEntry() {
//        this.g_PinEntryPro = PinEntryResult.BYPASS;
//        UROPElibJni.SetPinCVR(2);
//        this.procCVMcontinue();
//    }
//
//    public void onCheckCardisOut() {
//        byte[] buf = new byte[100];
//
//        while(true) {
//            int iret = UROPElibJni.ICCCheck(buf);
//            Log.i("maxlog", "ICCCheck 222 ret:" + iret);
//            if (iret != 0) {
//                return;
//            }
//
//            try {
//                Thread.sleep(100L);
//            } catch (InterruptedException var4) {
//                var4.printStackTrace();
//            }
//        }
//    }
//
//    public void checkCard(Hashtable<String, Object> data) {
//        MagManager mMagManager = new MagManager();
//        Hashtable<String, String> dataout = new Hashtable();
//        icoutMG = 0;
//        this.g_iret = 0;
//        g_checkCardRun = 0;
//        byte[] buf = new byte[100];
//        mMagManager.open();
//        long lcheckCardTimeout = 30000L;
//        if (data.containsKey("checkCardTimeout")) {
//            Object object = data.get("checkCardTimeout");
//            String string1 = object.toString();
//            lcheckCardTimeout = Long.valueOf(string1);
//        }
//
//        if (lcheckCardTimeout > 60000L || lcheckCardTimeout < 1000L) {
//            lcheckCardTimeout = 30000L;
//        }
//
//        Funs.startTimer(lcheckCardTimeout);
//
//        while(true) {
//            int iret = UROPElibJni.ICCCheck(buf);
//            byte[] StripInfo;
//            if (iret != 0) {
//                try {
//                    Thread.sleep(100L);
//                } catch (InterruptedException var16) {
//                    var16.printStackTrace();
//                }
//            } else {
//                StripInfo = new byte[100];
//                int[] atrlen = new int[10];
//                iret = UROPElibJni.IccReset(StripInfo, atrlen);
//                if (iret == 0) {
//                    ATR art = new ATR();
//                    iret = art.checkATR(StripInfo, atrlen[0]);
//                    if (iret != 0) {
//                        icoutMG = 3;
//                        this.onReturnCheckCardResult(CheckCardResult.NEED_FALLBACK, dataout);
//                        this.onCheckCardisOut();
//                        continue;
//                    }
//
//                    icoutMG = 0;
//                    g_checkCardRun = 1;
//                    dataout = new Hashtable();
//                    this.onReturnCheckCardResult(CheckCardResult.INSERTED_CARD, dataout);
//                    break;
//                }
//
//                dataout = new Hashtable();
//                this.onReturnCheckCardResult(CheckCardResult.NOT_ICC, dataout);
//                this.onCheckCardisOut();
//                ++icoutMG;
//                if (icoutMG >= 3) {
//                    dataout = new Hashtable();
//                    this.onReturnCheckCardResult(CheckCardResult.NEED_FALLBACK, dataout);
//                }
//            }
//
//            StripInfo = new byte[1024];
//            byte[] CardNo = new byte[20];
//            byte[] KSN = new byte[11];
//            byte[] chipTag = new byte[2];
//            if (this.g_iret == 1) {
//                this.onError(Error.DEVICE_BUSY, "cancel");
//                return;
//            }
//
//            iret = mMagManager.checkCard();
//            if (iret == 0) {
//                int allLen = mMagManager.getAllStripInfo(StripInfo);
//                if (allLen > 8) {
//                    int len = StripInfo[1];
//
//                    int i;
//                    for(i = 1; i < 24 && StripInfo[4 + len + i] != 61; ++i) {
//                        ;
//                    }
//
//                    chipTag[0] = StripInfo[4 + len + i + 5];
//                    System.arraycopy(StripInfo, 4 + len, CardNo, 0, i);
//                }
//
//                if (allLen < 16) {
//                    dataout = new Hashtable();
//                    this.onReturnCheckCardResult(CheckCardResult.BAD_SWIPE, dataout);
//                } else {
//                    if (chipTag[0] != 50 && chipTag[0] != 54) {
//                        dataout = new Hashtable();
//                        dataout.put("StripInfo", Funs.bytes2HexString(StripInfo, allLen));
//                        dataout.put("CardNo", (new String(CardNo)).trim());
//                        this.onReturnCheckCardResult(CheckCardResult.MSR, dataout);
//                        break;
//                    }
//
//                    if (icoutMG >= 3) {
//                        icoutMG = 0;
//                        dataout = new Hashtable();
//                        dataout.put("StripInfo", Funs.bytes2HexString(StripInfo, allLen));
//                        dataout.put("CardNo", (new String(CardNo)).trim());
//                        this.onReturnCheckCardResult(CheckCardResult.MSR, dataout);
//                        return;
//                    }
//
//                    dataout = new Hashtable();
//                    this.onReturnCheckCardResult(CheckCardResult.USE_ICC_CARD, dataout);
//                }
//            }
//
//            if (0 == Funs.checkTimer()) {
//                Log.i("maxlog", "time out");
//                this.onReturnCheckCardResult(CheckCardResult.TIMEOUT, dataout);
//                return;
//            }
//        }
//
//    }
//
//    public void powerOnIcc(Hashtable<String, Object> data) {
//        UROPElibJni.ICCOpen();
//    }
//
//    public void powerOffIcc() {
//        UROPElibJni.ICCClose();
//    }
//
//    public void sendApdu(Hashtable<String, Object> data) {
//        byte[] apdu = new byte[300];
//        int apdulen = 0;
//        Object object;
//        if (data.containsKey("apdu")) {
//            object = data.get("apdu");
//            String var3 = object.toString();
//        }
//
//        if (data.containsKey("apduLength")) {
//            object = data.get("apduLength");
//            apdulen = (Integer)data.get("apduLength");
//        }
//
//        int iret = UROPElibJni.sendApdu(apdu, apdulen / 2);
//        if (iret != 0) {
//            this.onReturnTransactionResult(TransactionResult.DEVICE_ERROR);
//        } else {
//            byte[] respApdu = new byte[300];
//            int[] rApdulen = new int[5];
//            UROPElibJni.responseApdu(respApdu, rApdulen);
//            String ssApdu = Funs.bytesToHexString(respApdu);
//            Hashtable<String, Object> ApduData = new Hashtable();
//            ApduData.put("apdu", ssApdu);
//            ApduData.put("apduLength", ssApdu.length() / 2);
//            this.onReturnApduResult(true, ApduData);
//        }
//    }
//
//    public void readTerminalSetting(String tag) {
//        byte[] strvalue = new byte[100];
//        int itag = (int)Long.valueOf(tag);
//        UROPElibJni.EmvGetTerminalSeting(itag, strvalue);
//        String value = new String(strvalue);
//        this.onReturnReadTerminalSettingResult(TerminalSettingStatus.SUCCESS, value);
//    }
//
//    public void updateTerminalSetting(String tlv) {
//        UROPElibJni.EmvUpdateTerminalSetting(tlv.getBytes());
//    }
//
//    public void getCAPKList() {
//    }
//
//    public void getCAPKDetail(String location) {
//    }
//
//    public static int tag_str_toTLV(byte[] ptr, int ptrlen, int tag, String str) {
//        int lenlen = 1;
//        int strlen = str.length();
//        byte[] tmpbuf = new byte[300];
//        tmpbuf = Funs.StrToHexByte(str);
//        ptr[ptrlen] = (byte)(tag >> 8 & 255);
//        ptr[ptrlen + 1] = (byte)(tag & 255);
//        if (128 <= strlen / 2) {
//            ptr[ptrlen + 2] = -127;
//            ptr[ptrlen + 3] = (byte)(strlen / 2);
//            lenlen = 2;
//        } else {
//            ptr[ptrlen + 2] = (byte)(strlen / 2);
//        }
//
//        System.arraycopy(tmpbuf, 0, ptr, ptrlen + 2 + lenlen, strlen / 2);
//        return ptrlen + 2 + lenlen + strlen / 2;
//    }
//
//    public static void updateCAPK(CAPK capk) {
//        byte[] pkbuf = new byte[300];
//        String strExptime = "20301231";
//        int len = false;
//        int len = 0;
//        int len = tag_str_toTLV(pkbuf, len, 40710, capk.rid);
//        len = tag_str_toTLV(pkbuf, len, 40738, capk.index);
//        len = tag_str_toTLV(pkbuf, len, 57092, capk.exponent);
//        len = tag_str_toTLV(pkbuf, len, 57093, strExptime);
//        len = tag_str_toTLV(pkbuf, len, 57090, capk.modulus);
//        len = tag_str_toTLV(pkbuf, len, 57091, capk.checksum);
//        UROPElibJni.updateCAPK(pkbuf, len);
//    }
//
//    public void findCAPKLocation(Hashtable<String, String> data) {
//    }
//
//    public void getEmvReportList() {
//    }
//
//    public void getEmvReport(String applicationIndex) {
//    }
//
//    public void readAID(String appIndex) {
//    }
//
//    public void updateAID1(Hashtable<String, String> data) {
//        byte[] aidbuf = new byte[300];
//        int len = 0;
//        String str = (String)data.get("aid");
//        int len = tag_str_toTLV(aidbuf, len, 40710, str);
//        str = (String)data.get("appVersion");
//        len = tag_str_toTLV(aidbuf, len, 40713, str);
//        str = (String)data.get("defaultTDOL");
//        str = (String)data.get("defaultDDOL");
//        len = tag_str_toTLV(aidbuf, len, 57108, str);
//        str = (String)data.get("contactlessTransactionLimit");
//        len = tag_str_toTLV(aidbuf, len, 57120, str);
//        str = (String)data.get("contactlessCVMRequiredLimit");
//        len = tag_str_toTLV(aidbuf, len, 57121, str);
//        str = (String)data.get("contactlessFloorLimit");
//        len = tag_str_toTLV(aidbuf, len, 57113, str);
//        str = (String)data.get("terminalFloorLimit");
//        str = (String)data.get("contactlessTransactionLimit");
//        len = tag_str_toTLV(aidbuf, len, 40710, str);
//        UROPElibJni.updateAID(aidbuf, len);
//    }
//
//    public void updateAID(Hashtable<String, String> data) {
//        byte[] aidbuf = new byte[300];
//        int len = 0;
//        String str = (String)data.get("aid");
//        int len = tag_str_toTLV(aidbuf, len, 40710, str);
//        str = (String)data.get("appVersion");
//        len = tag_str_toTLV(aidbuf, len, 40713, str);
//        str = "00";
//        len = tag_str_toTLV(aidbuf, len, 57089, str);
//        str = (String)data.get("contactTACDefault");
//        len = tag_str_toTLV(aidbuf, len, 57105, str);
//        str = (String)data.get("contactTACDenial");
//        len = tag_str_toTLV(aidbuf, len, 57107, str);
//        str = (String)data.get("contactTACOnline");
//        len = tag_str_toTLV(aidbuf, len, 57106, str);
//        str = (String)data.get("defaultDDOL");
//        len = tag_str_toTLV(aidbuf, len, 57108, str);
//        str = (String)data.get("terminalFloorLimit");
//        len = tag_str_toTLV(aidbuf, len, 40731, str);
//        UROPElibJni.updateAID(aidbuf, len);
//    }
//
//    public void clearAID() {
//        UROPElibJni.ClearAID();
//    }
//
//    public void clearCAPK() {
//        UROPElibJni.ClearCAPK();
//    }
//
//    public void onRequestTerminalTime() {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestTerminalTime();
//        }
//
//    }
//
//    public void onRequestSetAmount() {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestSetAmount();
//        }
//
//    }
//
//    public void onReturnAmountConfirmResult(boolean isConfirmed) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnAmountConfirmResult(isConfirmed);
//        }
//
//    }
//
//    public void onWaitingForCard(CheckCardMode checkCardMode) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onWaitingForCard(checkCardMode);
//        }
//
//    }
//
//    public void onReturnCancelCheckCardResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnCancelCheckCardResult(isSuccess);
//        }
//
//    }
//
//    void onReturnCheckCardResult(CheckCardResult checkCardResult, Hashtable<String, String> decodeData) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnCheckCardResult(checkCardResult, decodeData);
//        }
//
//    }
//
//    void onRequestSelectApplication(ArrayList<String> appList) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestSelectApplication(appList);
//        }
//
//    }
//
//    void onRequestPinEntry(PinEntrySource pinEntrySource) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestPinEntry(pinEntrySource);
//        }
//
//    }
//
//    void onRequestConfirmCardno() {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestConfirmCardno();
//        }
//
//    }
//
//    void onRequestFinalConfirm() {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestFinalConfirm();
//        }
//
//    }
//
//    void onRequestOnlineProcess(String tlv, String dataKsn) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestOnlineProcess(tlv, dataKsn);
//        }
//
//    }
//
//    void onReturnBatchData(String tlv) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnBatchData(tlv);
//        }
//
//    }
//
//    void onReturnReversalData(String tlv) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnReversalData(tlv);
//        }
//
//    }
//
//    void onReturnTransactionResult(TransactionResult transResult) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnTransactionResult(transResult);
//        }
//
//    }
//
//    void onRequestDisplayText(DisplayText displayText) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestDisplayText(displayText);
//        }
//
//    }
//
//    void onRequestClearDisplay() {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onRequestClearDisplay();
//        }
//
//    }
//
//    void onReturnEnableInputAmountResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEnableInputAmountResult(isSuccess);
//        }
//
//    }
//
//    void onReturnDisableInputAmountResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnDisableInputAmountResult(isSuccess);
//        }
//
//    }
//
//    void onReturnAmount(Hashtable<String, String> data) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnAmount(data);
//        }
//
//    }
//
//    void onReturnEnableAccountSelectionResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEnableAccountSelectionResult(isSuccess);
//        }
//
//    }
//
//    void onReturnDisableInputAmountResult2222(boolean isSuccess) {
//    }
//
//    void onReturnAccountSelectionResult(AccountSelectionResult result, int selectedAccountType) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnAccountSelectionResult(result, selectedAccountType);
//        }
//
//    }
//
//    void onReturnEmvCardDataResult(boolean isSuccess, String tlv) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEmvCardDataResult(isSuccess, tlv);
//        }
//
//    }
//
//    void onReturnEmvCardNumber(boolean isSuccess, String cardNumber) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEmvCardNumber(isSuccess, cardNumber);
//        }
//
//    }
//
//    void onReturnPowerOnIccResult(boolean isSuccess, String ksn, String atr, int atrLength) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnPowerOnIccResult(isSuccess, ksn, atr, atrLength);
//        }
//
//    }
//
//    void onReturnPowerOffIccResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnPowerOffIccResult(isSuccess);
//        }
//
//    }
//
//    void onReturnApduResult(boolean isSuccess, Hashtable<String, Object> data) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnApduResult(isSuccess, data);
//        }
//
//    }
//
//    void onReturnReadTerminalSettingResult(TerminalSettingStatus terminalSettingStatus, String tagValue) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnReadTerminalSettingResult(terminalSettingStatus, tagValue);
//        }
//
//    }
//
//    void onReturnUpdateTerminalSettingResult(TerminalSettingStatus terminalSettingStatus) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnUpdateTerminalSettingResult(terminalSettingStatus);
//        }
//
//    }
//
//    void onReturnCAPKList(List<CAPK> capkList) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnCAPKList(capkList);
//        }
//
//    }
//
//    void onReturnCAPKDetail(CAPK capk) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnCAPKDetail(capk);
//        }
//
//    }
//
//    void onReturnCAPKLocation(String location) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnCAPKLocation(location);
//        }
//
//    }
//
//    void onReturnUpdateCAPKResult(boolean isSuccess) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnUpdateCAPKResult(isSuccess);
//        }
//
//    }
//
//    void onReturnEmvReportList(Hashtable<String, String> data) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEmvReportList(data);
//        }
//
//    }
//
//    void onReturnEmvReport(String tlv) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnEmvReport(tlv);
//        }
//
//    }
//
//    void onReturnReadAIDResult(Hashtable<String, String> data) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnReadAIDResult(data);
//        }
//
//    }
//
//    void onReturnUpdateAIDResult(Hashtable<String, String> data) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onReturnUpdateAIDResult(data);
//        }
//
//    }
//
//    void onError(Error Error, String errorMessage) {
//        if (this.mEmvListener != null) {
//            this.mEmvListener.onError(Error, errorMessage);
//        }
//
//    }
//
//    public String getEMVLibVers() {
//        byte[] bvers = new byte[100];
//        int len = UROPElibJni.GetVers(bvers);
//        String buff = new String(bvers, 0, len);
//        return buff;
//    }
//
//    public String getEMVjarVers() {
//        String g_EMVJarVerInfo = "EMV2-jar V1.0.5";
//        return g_EMVJarVerInfo;
//    }
//
//    public int CheckKeyStatus() {
//        int iret = UROPElibJni.CheckKeyStatus();
//        return iret;
//    }
//}
