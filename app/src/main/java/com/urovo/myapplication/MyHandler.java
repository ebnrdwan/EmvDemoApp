package com.urovo.myapplication;

import android.device.SEManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.urovo.i9000s.api.emv.ContantPara;

import java.util.Hashtable;

import static com.urovo.myapplication.TestEmvActivityMain.mEmvApi;

/**
 * Created by lenovo on 2017/12/14.
 */

public class MyHandler extends Handler {

    public final static int iOnReturnCheckCardResultMag = 8;
    public final static int iOnReturnTransactionResult = 7;
    public final static int ionShowMsg = 6;
    public final static int ionProcessICC = 5;
    public final static int ionInputPIN = 4;


    SEManager mSEManager;

    public MyHandler(Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case iOnReturnCheckCardResultMag:
                String strResult = (String) msg.obj;
                strResult = (String) msg.obj;
                Toast.makeText(TestEmvActivityMain.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                break;
            case iOnReturnTransactionResult:
                int i = Log.i("maxlog", "iOnReturnTransactionResult");
                strResult = (String) msg.obj;
                Toast.makeText(TestEmvActivityMain.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                break;
            case ionShowMsg:
                strResult = (String) msg.obj;
                Toast.makeText(TestEmvActivityMain.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                Log.i("maxlog", "ionShowMsg");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case ionProcessICC: {
                strResult = (String) msg.obj;
                Hashtable<String, Object> data = new Hashtable<String, Object>();
                data = new Hashtable<String, Object>();
                mEmvApi.updateTerminalSetting("9F1A020356");
                data.put("checkCardMode", ContantPara.CheckCardMode.SWIPE_OR_INSERT);
                data.put("currencyCode", "818");
                data.put("amount", "20.00");
                data.put("cashbackAmount", "0");
                data.put("checkCardTimeout", "30");
                // TODO:  start emv
                mEmvApi.startEmv(data);
                Log.i("123", "TestEmvActivityMain  startEmv return ");
            }
            break;

        }
    }

}
