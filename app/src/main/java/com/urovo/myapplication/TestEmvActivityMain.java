package com.urovo.myapplication;

import android.content.Context;
import android.device.SEManager;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jniexport.UROPElibJni;
import com.urovo.i9000s.api.emv.CAPK;
import com.urovo.i9000s.api.emv.ContantPara;
import com.urovo.i9000s.api.emv.EmvApi;
import com.urovo.i9000s.api.emv.EmvListener;
import com.urovo.i9000s.api.emv.Funs;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**/

public class TestEmvActivityMain extends AppCompatActivity {
    public final static String LogTag = "emv";
    private Button btnStartEmv = null;
    private Button btnmsr = null;
    private TextView text1 = null;
    SEManager mSEManager;//= new SEManager();
    public static EmvApi mEmvApi = null;
    public static EmvListener mEmvListener = null;
    public static Context mContext = null;
    public static MyHandler mHandler;
    int pinType = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSEManager = new SEManager();
        mEmvListener = new MyEmvListener();
        param = new Bundle();
        mContext = this;
//  init_emvParam();
        byte[] ResponseData = new byte[100];//ldp test
        byte[] ResLen = new byte[10];
        byte[] keydata = new byte[100];
        int iret;
        keydata[0] = 1;
        //mHandlerPin = new MyHandler(mContext.getMainLooper());
        // TODO: 7/18/2018 AD initialize emv_listenert / api / handler 
        Log.i("applog", "downloadKey2:" + 111);


        Log.i("applog", "downloadKey2:" + 222);
        mEmvApi = new EmvApi(mEmvListener);
        Log.i("applog", "downloadKey2:" + 333);
        mHandler = new MyHandler(mContext.getMainLooper());
        Log.i("applog", "downloadKey2:" + 444);
        text1 = (TextView) findViewById(R.id.autoCompleteTextView2);


        text1.setText("emv result");
        //text1.clearComposingText();
        text1.setText("--emv result--");

        btnStartEmv = (Button) findViewById(R.id.btn_startEmv_id);
        btnStartEmv.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                // TODO: 7/18/2018 AD startEmVTest 
                TestEmv.my_testEmv();

            }
        });

        btnmsr = (Button) findViewById(R.id.button2);
        btnmsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //mEmvApi.set_handler(mHandler);

                new Thread(new Runnable() {
                    public void run() {
                        try {
                            // TODO: initi Capack
                            init_emvParam();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();


            }
        });

    }

    public void init_emvParam() {
        TestEmv.initAID_CAPK();
    }

    Bundle param;

    public void doOfflinePin() {
        String cardno = mEmvApi.getValByTag((int) 0x5A);

        String Track2 = mEmvApi.getValByTag((int) 0x57);
//        Log.i("maxlog", "bundle.getByteArray pinBlock:" + Funs.bytes2HexString(Track2, 19));
        String cmvList = mEmvApi.getValByTag((int) 0x8E);
        if ((cardno.charAt(cardno.length() - 1) == 'f') || (cardno.charAt(cardno.length() - 1) == 'F'))
            cardno = cardno.substring(0, cardno.length() - 1);
        this.mSEManager = new SEManager();
        this.param = new Bundle();
        this.param.putInt("KeyUsage", 3);
        this.param.putInt("PINKeyNo", 1);
        this.param.putInt("pinAlgMode", 1);
        this.param.putString("cardNo", cardno);
        this.param.putBoolean("sound", true);
        this.param.putBoolean("onlinePin", false);
        this.param.putBoolean("FullScreen", true);
        this.param.putLong("timeOutMS", 60000L);
        this.param.putString("supportPinLen", "0,4,6,8,10,12");
        this.param.putString("title", "Security Keyboard");
        this.param.putString("message", "please input password ");
        this.pinType = 1;
        this.mSEManager.getPinBlockEx(this.param, mPedInputListenerolp);
    }

    public void doOnlinePin() {
        Log.i("maxlog", "doOnlinePin");

        //mSEManager  = new SEManager();
        param.putInt("KeyUsage", 0x02);
        param.putInt("PINKeyNo", 1);
        param.putInt("pinAlgMode", 4);// 5 dupkt

        String Track2 = mEmvApi.getValByTag((int) 0x57);
//        Log.i("maxlog", "bundle.getByteArray pinBlock:" + Funs.bytes2HexString(Track2, 19));
        String cardno = mEmvApi.getValByTag((int) 0x5A);
        String cmvList = mEmvApi.getValByTag((int) 0x8E);

        if ((cardno.charAt(cardno.length() - 1) == 'f') || (cardno.charAt(cardno.length() - 1) == 'F'))
            cardno = cardno.substring(0, cardno.length() - 1);

        Log.i("maxlog", "doOnlinePin cardno " + cardno);
        param.putString("cardNo", cardno);
        param.putBoolean("sound", true);
        param.putBoolean("onlinePin", true);
        param.putBoolean("FullScreen", true);
        param.putLong("timeOutMS", 60000);
        param.putString("supportPinLen", "0,4,5,6,7,8,9,10,11,12");
        param.putString("title", "Security Keyboard");
        param.putString("message", "please input password ");
        Log.i("maxlog", "getPinBlockEx ");

        // TODO: create Bin Block
        mSEManager.getPinBlockEx(param, mPedInputListenerolp);
    }

    private SEManager.PedInputListener mPedInputListenerolp = new SEManager.PedInputListener() {
// TODO: get Bin Block creation results

        @Override
        public void onChanged(int i, int i1, byte[] bytes) {
            int result = i;
            int length = i1;
            Log.i("maxlog", "bundle.getByteArray onChanged");
            if (result == 0) {
                // TODO: Bin block created // way to go for online pin
                Log.i("maxlog", "bundle.getByteArray ");

                //doOnlinePin();

                final byte[] pinBlock = bytes;//bundle.getByteArray("pinBlock");
                //                //final byte[] ksn = bundle.getByteArray("ksn");
                //                //                //final byte[] ksn = bundle.getByteArray("ksn");

                if (pinBlock.length > 0 && pinBlock.length <= 4)
                    Log.i("maxlog", "bundle.getByteArray pinBlock:" + Funs.bytes2HexString(pinBlock, 4));
                //Run1 = true;
                //if(ksn!=null)
                //    Log.i("maxlog", "bundle.getByteArray ksn:"+Funs.bytes2HexString(ksn, 10));

                mEmvApi.sendPinEntryResult("");
            } else {
                Log.i("maxlog", "result = " + result + " length = " + length);
            }

            if (result == 1) {
                // TODO: Bin block not created // cancel go to next process in CVMLIST

                mEmvApi.cancelPinEntry();
//                mEmvApi.doOfflinePin();

            } else if (result == 2) {
                mEmvApi.bypassPinEntry();
            }
        }
    };

    class MyEmvListener implements EmvListener {

        @Override
        public void onRequestTerminalTime() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestTerminalTime");
        }

        @Override
        public void onRequestSetAmount() {
            // TODO: show dialog to must set amount
            Log.i(LogTag, "TestEmvActivityMain  onRequestSetAmount");
        }

        @Override
        public void onReturnAmountConfirmResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAmountConfirmResult");
        }

        @Override
        public void onWaitingForCard(ContantPara.CheckCardMode checkCardMode) {
            Log.i(LogTag, "TestEmvActivityMain  onWaitingForCard checkCardMode=" + checkCardMode);
//        tag ="x9F\x33"    offline E0\xF8\xC8  // online E0E1C8
            mEmvApi.updateTerminalSetting("9F3303E0B0C8");
        }

        @Override
        public void onReturnCancelCheckCardResult(boolean isSuccess) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCancelCheckCardResult isSuccess=" + isSuccess);
            UROPElibJni.CheckKeyStatus();

        }

        @Override
        public void onReturnCheckCardResult(ContantPara.CheckCardResult checkCardResult, Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCheckCardResult checkCardResult =" + checkCardResult);
            Log.d(LogTag, hashtable.toString());
            if (checkCardResult == ContantPara.CheckCardResult.MSR) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.ionShowMsg;
                tempMsg.obj = " MAG CARD SWIPE SUCCESS  !";
                mHandler.sendMessage(tempMsg);
            } else if (checkCardResult == ContantPara.CheckCardResult.INSERTED_CARD) {
                mEmvApi.updateTerminalSetting("9F3303E0F8C8");
                // TODO: 1- detected the card insert
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.ionShowMsg;
                tempMsg.obj = " ICC INSERTED  !";
                mHandler.sendMessage(tempMsg);//ionShowMsg

                Message tempMsg2 = mHandler.obtainMessage();
                tempMsg2.what = MyHandler.ionProcessICC;
                tempMsg2.obj = " Process icc";
                mHandler.sendMessage(tempMsg2);//ionProcessICC

            } else if (checkCardResult == ContantPara.CheckCardResult.NEED_FALLBACK) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.ionShowMsg;
                tempMsg.obj = " NEED_FALLBACK !";
                mHandler.sendMessage(tempMsg);

            } else if (checkCardResult == ContantPara.CheckCardResult.BAD_SWIPE) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.ionShowMsg;
                tempMsg.obj = " BAD_SWIPE !";
                mHandler.sendMessage(tempMsg);
            } else if (checkCardResult == ContantPara.CheckCardResult.NOT_ICC) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.ionShowMsg;
                tempMsg.obj = " NOT_ICC, A chip card is swiped. !";
                mHandler.sendMessage(tempMsg);

                Log.i("maxlog", "runOnUiThread-----NOT_ICC");
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i("maxlog", "3000 ms ok-----------");

        }

        @Override
        public void onRequestSelectApplication(ArrayList<String> arrayList) {
            // TODO: 2- got list of applications
            Log.i(LogTag, "TestEmvActivityMain  onRequestSelectApplication");
            int i;
            for (i = 0; i < arrayList.size(); i++) {
                Log.d(LogTag, "app name " + i + " : " + arrayList.get(i));

            }
            //block

            if (i == 1)
                mEmvApi.selectApplication(0);
            else
                mEmvApi.selectApplication(0);//multi app`

            // break;
        }

        @Override
        public void onError(ContantPara.Error error, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onError:" + error);
            Log.i(LogTag, "TestEmvActivityMain  s:" + s);
        }

        @Override
        public void onRequestPinEntry(ContantPara.PinEntrySource pinEntrySource) {

            // TODO: 3-  request pin entry  for online pin
            Log.i(LogTag, "TestEmvActivityMain  onRequestPinEntry");

            if (pinEntrySource == ContantPara.PinEntrySource.KEYPAD) {

//                doOnlinePin();
                doOfflinePin();
                Log.i(LogTag, "TestEmvActivityMain go for OfflinePin ");
            } else {

            }

        }

        public void onRequestConfirmCardno() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestConfirmCardno-----------");
            mEmvApi.sendConfirmCardnoResult(true);

        }

        @Override
        public void onRequestFinalConfirm() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestFinalConfirm-----------");
            mEmvApi.sendFinalConfirmResult(true);

        }

        @Override
        public void onRequestOnlineProcess(String cardTlvData, String dataKsn) {

            String equivelantTrack2Data = mEmvApi.getValByTag((int) 0x57);
            String cardHolderName = mEmvApi.getValByTag((int) 0x5F20);
            String expiration = mEmvApi.getValByTag((int) 0x5F24);
            String currency = mEmvApi.getValByTag((int) 0x5F2A);


            // TODO: 4- got card data process online

            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess");
            //send s to sever
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess iccdata: " + cardTlvData);
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess dataKsn: " + dataKsn);
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess EmvTrack2: " + equivelantTrack2Data);
// 4761739001010119d22122011758928889
            /* set icc data on the text*/
            text1.setText("iccdata:" + cardTlvData + "  KSN:" + dataKsn);

            // TODO: wha-1 =  what is the responseData
            String responseData = "8A023035";//just for test  //server return data

            mEmvApi.sendOnlineProcessResult(cardTlvData);

        }

        @Override
        public void onReturnBatchData(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnBatchData");
        }

        @Override
        public void onReturnReversalData(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnReversalData");
        }

        @Override
        public void onReturnTransactionResult(ContantPara.TransactionResult transactionResult) {
// TODO: handle online pin
            Log.i(LogTag, "TestEmvActivityMain  onReturnTransactionResult  transactionResult=" + transactionResult);
            //mHandler = new MyHandler(mContext.getMainLooper());
            if (transactionResult == ContantPara.TransactionResult.APPROVED) {
                Log.i(LogTag, "TestEmvActivityMain onReturnTransactionResult  APPROVED");

                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = APPROVED !";
                String str = text1.getText().toString();
                text1.setText(str + "   " + tempMsg.obj);

                mHandler.sendMessage(tempMsg);
            } else if (transactionResult == ContantPara.TransactionResult.DECLINED) {
                Log.i(LogTag, "TestEmvActivityMain onReturnTransactionResult  DECLINED");

                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = DECLINED !";

                String str = text1.getText().toString();
                text1.setText(str + "   " + tempMsg.obj);


                mHandler.sendMessage(tempMsg);
//                mEmvApi.doOfflinePin();
            }

            if (transactionResult == ContantPara.TransactionResult.TERMINATED) {
                Log.i(LogTag, " TestEmvActivityMain onReturnTransactionResult  TERMINATED");
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = MyHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = TERMINATED !";
                mHandler.sendMessage(tempMsg);
            }

        }

        @Override
        public void onRequestDisplayText(ContantPara.DisplayText displayText) {
            Log.i(LogTag, "TestEmvActivityMain  onRequestDisplayText");
        }

        @Override
        public void onRequestClearDisplay() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestClearDisplay");
        }

        @Override
        public void onReturnEnableInputAmountResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEnableInputAmountResult");
        }

        @Override
        public void onReturnDisableInputAmountResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnDisableInputAmountResult");
        }

        @Override
        public void onReturnAmount(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAmount");
        }

        @Override
        public void onReturnEnableAccountSelectionResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEnableAccountSelectionResult");
        }

        @Override
        public void onReturnAccountSelectionResult(ContantPara.AccountSelectionResult accountSelectionResult, int i) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAccountSelectionResult");
        }

        @Override
        public void onReturnEmvCardDataResult(boolean b, String s) {
            Log.i(LogTag, s);
            Log.i(LogTag, "TestEmvActivityMain  onReturnPinEntryResult");
            Log.i(LogTag, s);
        }

        @Override
        public void onReturnEmvCardNumber(boolean b, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvCardDataResult");
            Log.i(LogTag, s);
        }

        @Override
        public void onReturnPowerOnIccResult(boolean b, String s, String s1, int i) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvCardNumber");
            Log.i(LogTag, s + s1);
        }

        @Override
        public void onReturnPowerOffIccResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEncryptPinResult");
        }

        @Override
        public void onReturnApduResult(boolean b, Hashtable<String, Object> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEncryptDataResult");
        }

        @Override
        public void onReturnReadTerminalSettingResult(ContantPara.TerminalSettingStatus terminalSettingStatus, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnPowerOnIccResult");
            Log.i(LogTag, s);
        }

        @Override
        public void onReturnUpdateTerminalSettingResult(ContantPara.TerminalSettingStatus terminalSettingStatus) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnPowerOffIccResult");
        }

        @Override
        public void onReturnCAPKList(List<CAPK> list) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnApduResult");
            Log.i(LogTag, list.toString());
        }

        @Override
        public void onReturnCAPKDetail(CAPK capk) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnInjectSessionKeyResult");
        }

        @Override
        public void onReturnCAPKLocation(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnReadTerminalSettingResult");
        }

        @Override
        public void onReturnUpdateCAPKResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnUpdateTerminalSettingResult");
        }

        @Override
        public void onReturnEmvReportList(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKList");
            Log.i(LogTag, hashtable.toString());
        }

        @Override
        public void onReturnEmvReport(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKDetail");
            Log.i(LogTag, s);
        }

        @Override
        public void onReturnReadAIDResult(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKLocation");
            Log.i(LogTag, hashtable.toString());
        }

        @Override
        public void onReturnUpdateAIDResult(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvReportList");
            Log.i(LogTag, hashtable.toString());
        }

    }

    ;


}
