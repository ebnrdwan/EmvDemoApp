package com.urovo.myapplication;

import android.device.SEManager;
import android.util.Log;

import com.urovo.i9000s.api.emv.CAPK;
import com.urovo.i9000s.api.emv.ContantPara;

import java.util.Hashtable;

import static com.urovo.myapplication.TestEmvActivityMain.LogTag;
import static com.urovo.myapplication.TestEmvActivityMain.mEmvApi;


/**
 * Created by lenovo on 2017/11/26.
 */

public class TestEmv {

    static int icount = 0;
    static int icoutMG = 0;


    private static void addCAPK_ADVT_92() {
        // TODO: 7/18/2018 AD capk 
        CAPK capk = new CAPK();
        //capk.location = "30";
        capk.rid = "A000000003";
        capk.index = "92";
        capk.exponent = "03";
        //capk.size = "07C0";
        capk.modulus = "996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9F";
        capk.checksum = "429C954A3859CEF91295F663C963E582ED6EB253";
        mEmvApi.updateCAPK(capk);

    }

    private static void addCAPK_ADVT_94() {
        CAPK capk = new CAPK();

        capk.rid = "A000000003";
        capk.index = "94";
        capk.exponent = "03";

        capk.modulus = "ACD2B12302EE644F3F835ABD1FC7A6F62CCE48FFEC622AA8EF062BEF6FB8BA8BC68BBF6AB5870EED579BC3973E121303D34841A796D6DCBC41DBF9E52C4609795C0CCF7EE86FA1D5CB041071ED2C51D2202F63F1156C58A92D38BC60BDF424E1776E2BC9648078A03B36FB554375FC53D57C73F5160EA59F3AFC5398EC7B67758D65C9BFF7828B6B82D4BE124A416AB7301914311EA462C19F771F31B3B57336000DFF732D3B83DE07052D730354D297BEC72871DCCF0E193F171ABA27EE464C6A97690943D59BDABB2A27EB71CEEBDAFA1176046478FD62FEC452D5CA393296530AA3F41927ADFE434A2DF2AE3054F8840657A26E0FC617";
        capk.checksum = "C4A3C43CCF87327D136B804160E47D43B60E6E0F";
        mEmvApi.updateCAPK(capk);
    }

    private static void addCAPK_mast_F1() {
        CAPK capk = new CAPK();

        capk.rid = "A000000004";
        capk.index = "F1";
        capk.exponent = "03";

        capk.modulus = "A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7";
        capk.checksum = "D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BB";
        mEmvApi.updateCAPK(capk);
    }

    private static void addCAPK_mast_F8() {
        CAPK capk = new CAPK();

        capk.rid = "A000000004";
        capk.index = "F8";
        capk.exponent = "03";

        capk.modulus = "A1F5E1C9BD8650BD43AB6EE56B891EF7459C0A24FA84F9127D1A6C79D4930F6DB1852E2510F18B61CD354DB83A356BD190B88AB8DF04284D02A4204A7B6CB7C5551977A9B36379CA3DE1A08E69F301C95CC1C20506959275F41723DD5D2925290579E5A95B0DF6323FC8E9273D6F849198C4996209166D9BFC973C361CC826E1";
        capk.checksum = "00000000000000000000000000000000000000";
        mEmvApi.updateCAPK(capk);
    }

    public static void my_testAid2() {

        Hashtable<String, String> data = new Hashtable<String, String>();
        data.put("aid", "A0000000032010");
        data.put("appVersion", "008C");
        data.put("terminalFloorLimit", "00000000");
        data.put("contactTACDefault", "DC4000A800");
        data.put("contactTACDenial", "0010000000");
        data.put("contactTACOnline", "DC4004F800");
        data.put("defaultDDOL", "9F3704");
        mEmvApi.updateAID(data);
    }

    public static void my_testAid() {

        Log.i("maxlog", "my_testAid mastcard AID");

        Hashtable<String, String> data = new Hashtable<String, String>();
        data.put("aid", "A0000000041010");
        data.put("appVersion", "0002");
        data.put("terminalFloorLimit", "00000000");
        data.put("contactTACDefault", "0000000000");
        data.put("contactTACDenial", "0000000000");
        data.put("contactTACOnline", "0000000000");

        data.put("defaultDDOL", "9F3704");

        // A0000000043060D0562222
        mEmvApi.updateAID(data);

        data.put("aid", "A0000000043060");
        mEmvApi.updateAID(data); //Maestro

        /*data.put("aid", "A00000002501");
        mEmvApi.updateAID(data);
        data.put("aid", "A0000000651010");
        mEmvApi.updateAID(data);

        data.put("aid", "A0000000043060D0561111");
        mEmvApi.updateAID(data);//A0000000046000`
        data.put("aid", "A0000000043060D0562222");
        mEmvApi.updateAID(data);//A0000000046000
        data.put("aid", "A0000000046000");
        mEmvApi.updateAID(data);//*/

    }

    public static void my_testAidAddVISA() {

        Log.i("maxlog", "my_testAidAddVISA");

        Hashtable<String, String> data = new Hashtable<String, String>();
        data.put("aid", "A0000000031010");
        data.put("appVersion", "008C");
        data.put("terminalFloorLimit", "00000000");
        data.put("contactTACDefault", "DC4000A800");
        data.put("contactTACDenial", "0010000000");
        data.put("contactTACOnline", "DC4004F800");

        //data.put("defaultTDOL", "F850A8F800");
        data.put("defaultDDOL", "9F3704");
        mEmvApi.updateAID(data);

        data.put("aid", "A000000003101001");
        mEmvApi.updateAID(data);
        data.put("aid", "A000000003101002");
        mEmvApi.updateAID(data);
        data.put("aid", "A000000003101003");
        mEmvApi.updateAID(data);
        data.put("aid", "A000000003101004");
        mEmvApi.updateAID(data);
        data.put("aid", "A000000003101005");
        mEmvApi.updateAID(data);
        data.put("aid", "A0000003330101");//uninpay
        mEmvApi.updateAID(data);

    }

    public static void initAID_CAPK() {
//
        addCAPK_ADVT_92();
        addCAPK_ADVT_94();
        addCAPK_mast_F1();
        addCAPK_mast_F8();
        my_testAid();
        my_testAidAddVISA();
    }


    public static void my_testEmv() {

        Log.i(LogTag, "TestEmvActivityMain  my_testEmv");
        String libvers;
        String jarvers;

        //mEmvApi.clearAID();
        //mEmvApi.clearCAPK();
//        tag ="x9F\x33"    offline E0\xF8\xC8  // online E0E1C8
        mEmvApi.updateTerminalSetting("9F3303E0F8C8");
        libvers = mEmvApi.getEMVLibVers();
        jarvers = mEmvApi.getEMVjarVers();

        Log.d("testlog", "libvers:" + libvers);
        Log.d("testlog", "jarvers:" + jarvers);

        SEManager mSeManager = new SEManager();
        byte[] ResponseData = new byte[256];
        byte[] ResLen = new byte[256];

        //mSeManager.getStatus(ResponseData, ResLen);
        //Log.d("applog", Funs.bytes2HexString(ResponseData, 10));

        int iret1 = mEmvApi.CheckKeyStatus();
        Log.d("applog", "mEmvApi.CheckKeyStatus iret1:" + iret1);

//        int iret;
//        //if(icount==0)
//        if(false)
//        {
//            SEManager mSEManager = new SEManager();
//
//            //byte[] bsBdk = Funs.StrToHexByte("620CA71586367D928F3DABA4B9B79C9F");
//            byte[] bsBdk = Funs.StrToHexByte("620CA71586367D928F3DABA4B9B79C9F");
//            byte[] bsBdk2 = Funs.StrToHexByte("620CA71586367D928F3DABA4B9B79C9F");
//            byte[] bsKsn = Funs.StrToHexByte("11111746011BEDE00001");
//            byte[] bsIpek = new byte[32];//Funs.StrToHexByte("397126A1B93150C5D54B0CC672F89EA5");
//            int ret = mSEManager.downloadKeyDukpt(1, bsBdk, bsBdk.length, bsKsn, bsKsn.length, bsIpek, 0);//DATA
//            int ret2 = mSEManager.downloadKeyDukpt(3, bsBdk2, bsBdk2.length, bsKsn, bsKsn.length, bsIpek, 0);//PIN
//            icount++;
//        }


        new Thread(new Runnable() {
            public void run() {
                try {
                    //TestEmv.my_testEmv();
                    Hashtable<String, Object> data = new Hashtable<String, Object>();
                    // TODO: 1.1  set methodology for reading data
                    data.put("checkCardMode", ContantPara.CheckCardMode.SWIPE_OR_INSERT);
                    data.put("checkCardTimeout", "50000");
                    // TODO: 1.2 check Card data
                    mEmvApi.checkCard(data);
                    //TestEmvActivityMain.jar  onReturnCheckCardResult
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        Log.i(LogTag, "TestEmvActivityMain  return ");

    }
//    public static int g_is_icc = 0;

}
